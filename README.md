# Guess Number Game

This is a simple number guessing game built with HTML, CSS, and JavaScript. It features:

- **Infinite levels:** The game continues to increase in difficulty as you progress.
- **Life system:** You have a limited number of lives (default 10) to guess correctly.
- **Points and multipliers:** Earn points for correct guesses and increase your multiplier to boost your score.
- **Settings:** Customize the game by increasing your life count or multiplier using your accumulated points.
- **Analysis:** Track your performance with a line chart showing the number of attempts per level.
- **Local storage:** Your settings and high score are saved locally in your browser.

## How to Play

1. **Game Tab:**
   - Input a number within the given range.
   - Click "Try" or press the spacebar to submit your guess.
   - Hints will be provided in the "Remarks" section.
   - For every incorrect guess, you lose a life.
   - The game ends when you run out of lives.

2. **Settings Tab:**
   - Use your accumulated points to upgrade your multiplier or increase your life count.
   - The higher the multiplier, the faster you accumulate points.

3. **Analysis Tab:**
   - View a line chart displaying your performance (number of attempts per level).

## Code Overview

- **HTML:** Sets up the basic structure of the game interface, including input fields, buttons, and divs for displaying information.
- **CSS:** Styles the game elements, including fonts, colors, and layout.
- **JavaScript:**
    - Handles game logic, including generating random numbers, checking guesses, updating scores, and managing lives.
    - Uses jQuery for tab functionality and DOM manipulation.
    - Uses Chart.js to create the analysis chart.
    - Employs local storage to save game settings and progress.

## Key Features

- **Infinite Levels:** The game provides endless challenges with increasing difficulty.
- **Life System:** Adds a strategic element to the gameplay, requiring careful consideration of guesses.
- **Points and Multipliers:** Motivates players to improve their performance and achieve higher scores.
- **Settings:** Allows for customization and strategic upgrades to enhance gameplay.
- **Analysis:** Provides visual feedback on player performance, enabling them to track their progress.
- **Local Storage:** Ensures that player progress and settings are saved for future sessions.

## Future Improvements

- **Visual Enhancements:** Improve the game's aesthetics with more engaging graphics and animations.
- **Sound Effects:** Add sound effects to enhance the gaming experience.
- **Mobile Optimization:** Ensure the game is fully responsive and playable on mobile devices.

## Contributing

Contributions are welcome! Feel free to submit bug reports, feature requests, or pull requests.



## Demo

[Live Demo of the Game Number](https://guessnumber-74bb34.gitlab.io/) 